# MessageGuard #

Users' data should be under the control of the user who authored it. MessageGuard is a tool that allows for ubiquitous encryption of data accross the web. This allows users to share their information safely without worrying that unintended viewers, including the service providers, might see it.

Message Guard is also a framework for exploring what are the best ways to encrypt messages on the web. Researchers can use this framework to test out new systems and find what is best for users.

### What is this repository for? ###

This repository is maintained by the Internet Security Research Lab. We are open to collaboration with anyone that would like to increase the functionality of Message Guard.

### Who do I talk to? ###
For questions or comments contact Scott Ruoti <ruoti@isrl.byu.edu>.